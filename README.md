# Wallet 

## Objetivos
  - Criar um SPA para consumir as API's do projeto walletFront
  
## Instruções para rodar
  - Rodar o comando npm install
  >Porta utilizada para frontend: 3000
  - Rodar o comando npm start

## Entrar na autenticacao
  - Banco de dados populado apenas com um alguns usuários (vide walletBack/database/seed)
    - Username: 1111
    - Password: otavio
