import { createSlice, configureStore } from '@reduxjs/toolkit';
import axios from 'axios';

export const login = createSlice({
  name: 'name',
  initialState: {
    nameUser: JSON.parse(localStorage.getItem('credentials')) ? JSON.parse(localStorage.getItem('credentials')).name : null,
  },
  reducers: {
    loginUser: (state, action) => {
      state.nameUser = action.payload.name;
      axios.defaults.headers.common['Authorization'] = `bearer ${action.payload.token}`;
    },
    logoffUser: state => {
      delete axios.defaults.headers.common['Authorization'];
      localStorage.clear();
      state.nameUser = false;
    },
  }
})

export const { loginUser, logoffUser } = login.actions

const store = configureStore({
  reducer: login.reducer
})

export default store
