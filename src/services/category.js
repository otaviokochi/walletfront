import axios from 'axios';

export const getCategories = async () => {
  const url = `http://localhost:5001/categories`;
  return await axios.get(url);
}

export const createCategory = async (category) => {
  const url = `http://localhost:5001/categories`;
  return await axios({
    method: 'POST',
    url,
    data: {
      ...category,
    }
  })
}

export const updateCategory = async (category) => {
  const url = `http://localhost:5001/categories`;
  return await axios({
    method: 'PATCH',
    url,
    data: {
      ...category
    }
  })
}
