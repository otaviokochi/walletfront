const axios = require('axios');

const signup = async user => {
  const url = 'http://localhost:5001/wallets'
  return await axios({
    method: 'POST',
    url: url,
    data: user,
  })

}

export default signup;