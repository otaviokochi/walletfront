import axios from 'axios'

const loginRequest = async (cpf, password) => {
  const url = 'http://localhost:5001/signin';
  return await axios({
    method: 'POST',
    url: url,
    data: {
      cpf,
      password
    }
  })
}

export default loginRequest;