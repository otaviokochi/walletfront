import axios from 'axios';

export const createPayment = async (payment) => {
  const url = `http://localhost:5001/payments`;
  return await axios({
    method: 'POST',
    url,
    data: {
      ...payment,
    }
  })
}

export const getPayment = async () => {
  const url = `http://localhost:5001/payments`;
  return await axios.get(url);
}

export const pay = async (id) => {
  const url = `http://localhost:5001/payments/${id}`;
  return await axios({
    method: 'POST',
    url,
  })
}

export const chargeBack = async (id) => {
  const url = `http://localhost:5001/payments/${id}/estorno`;
  return await axios({
    method: 'PUT',
    url,
  })
}