import axios from 'axios';

export const getMoney = async (cpf) => {
  const url = `http://localhost:5001/wallets/money`;
  return await axios.get(url);
}
