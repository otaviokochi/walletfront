import axios from 'axios';

export const newTransaction = async (values) => {
  const url = `http://localhost:5001/transactions`;
  return await axios.post(url, {...values});
}

export const getTransaction = async (inicialDate = false, finalDate = false) => {
  let url;
  if (inicialDate && finalDate)
    url = `http://localhost:5001/transactions?initialDate=${inicialDate}&finalDate=${finalDate}`;
  else 
    url = `http://localhost:5001/transactions`;
  return await axios.get(url);
}

export const downloadCSV = async (transactions) => {
  transactions.forEach(transaction => delete transaction.id)
  const url = `http://localhost:5001/transactions/csv`;
  return await axios({
    method: 'POST',
    url: url,
    data: {
      transactions
    },
  })}
