import { Layout } from 'antd'
import React from 'react'
import { Route, Switch } from "react-router-dom";

import MakeTransaction from '../../views/client/MakeTransaction'
import ViewTransaction from '../../views/client/ViewTransaction'
import CreateCategory from '../../views/client/CreateCategory'
import UpdateCategory from '../../views/client/UpdateCategory'
import CreatePayment from '../../views/client/CreatePayment'
import ViewPayment from '../../views/client/ViewPayment'


import './Content.css'

const Content = () => {
  const { Content } = Layout 

  return (
  <Layout>
    <Content
    className="site-layout-background"
    style={{
      padding: 24,
      margin: 0,
    }}>
      <Switch>
        <Route path="/makeTransaction">
          <MakeTransaction />
        </Route>
        <Route path="/viewTransaction">
          <ViewTransaction />
        </Route>
        <Route path="/createCategory">
          <CreateCategory />
        </Route>
        <Route path="/updateCategory">
          <UpdateCategory />
        </Route>
        <Route path="/createPayment">
          <CreatePayment />
        </Route>
        <Route path="/viewPayment">
          <ViewPayment />
        </Route>
      </Switch>
    </Content>
  </Layout>
  )
}

export default Content;