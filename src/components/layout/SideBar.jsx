import React from 'react'
import { Menu, Layout } from 'antd'
import { Link } from "react-router-dom";


const SideBar = () => {
  const { Sider } = Layout;
  const { SubMenu } = Menu;

  return (
    <Sider width={200} style={{ background: 'white' }}>
      <Menu  
        mode="inline"
        // defaultSelectedKeys={['createUser']}
        // defaultOpenKeys={['user']}
        >
        <SubMenu key="user" title="Opções">
          <Menu.Item style={{padding: "0px", margin: "2px"}} key="makeTransaction" >
            <Link to ="/makeTransaction">Realizar Transferência</Link>
          </Menu.Item>
          <Menu.Item key="viewTransaction" style={{padding: "0px", margin: "2px"}}>
            <Link to ="/viewTransaction">Ver Transferências Realizadas</Link>
          </Menu.Item>
          <Menu.Item key="createCategory" style={{padding: "0px", margin: "2px"}}>
            <Link to ="/createCategory">Criar Categoria</Link>
          </Menu.Item>
          <Menu.Item key="updateCategory" style={{padding: "0px", margin: "2px"}}>
            <Link to ="/updateCategory">Atualizar Categoria</Link>
          </Menu.Item>
          <Menu.Item key="createPayment" style={{padding: "0px", margin: "2px"}}>
            <Link to ="/createPayment">Criar Pagamentos</Link>
          </Menu.Item>
          <Menu.Item key="viewPayment" style={{padding: "0px", margin: "2px"}}>
            <Link to ="/viewPayment">Ver Pagamentos</Link>
          </Menu.Item>
        </SubMenu>
      </Menu>
    </Sider>
  )
}

export default SideBar;