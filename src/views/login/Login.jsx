import React, { useState } from 'react';
import { Row, Col, Card, Form, Button, Input, Typography } from 'antd'
import loginRequest from '../../services/login'
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { useHistory } from 'react-router-dom';
import apiSignUp from '../../services/signup.js';
import store, { loginUser } from '../../redux/auth';
import './Login.css';


const Login = () => {
  const history = useHistory();
  const [succesLogin, setSuccesLogin] = useState(true);
  const [succesSignup, setSuccesSignup] = useState(false);
  const [signup, setSignup] = useState(false);
  const [firstRender, setFirstRender] = useState(true);
  const [error, setError] = useState(false);

  const finishSignup = async values => {
    console.log(values)
    const signupRequest = await apiSignUp(values)
      .then(response => {
        setSuccesSignup(true);
      })
      .catch(error => {
        console.log(error);
        if (error && error.response && error.response.data && error.response.data.message)
          setError(error.response.data.message);
        setSuccesSignup(false);
        setFirstRender(false);
      })
  }

  const reset = (e) => {
    setFirstRender(true);
    setSignup(false);
    setSuccesSignup(false);
    setSuccesLogin(true);
  }

  const onFinish = (values) => {
    loginRequest(values.cpf, values.password)
      .then(response => {
        store.dispatch(loginUser(response.data));
        localStorage.setItem('credentials', JSON.stringify(response.data));
        history.push("/");
      })
      .catch(() => setSuccesLogin(false));
  }

  return (
    <Row className='loginRow' align='middle'>
      <Card headStyle={{ borderColor: "rgba(120, 120, 120)" }}
        style={{
          borderColor: "rgba(120, 120, 120)",
          width: 310,
        }}
        title={
          <Typography.Title
            level={3}
            style={{
              width: '100%',
              textAlign: 'center',
              color: '#000'
            }}>
            Carteira Virtual
              </Typography.Title>
        }
        size="large"
      >
        {signup &&
          <Row>
            {succesSignup &&
              <Col span={24}>
                <h2>Usuário Criado com Sucesso!</h2>
              </Col>
            }
            {!succesSignup && !firstRender &&
              <Row>
                {!error &&
                  <Col span={24}>
                    <h2>Erro ao criar usuário!</h2>
                  </Col>
                }
                {error &&
                  <Col span={24}>
                    <h2>{error}</h2>
                  </Col>
                }
              </Row>
            }
            <Col span={24}>
              <Form layout="vertical" name="authentication" onFinish={finishSignup}>
                <Form.Item name="cpf" label="Cpf" rules={[{ required: true, message: "Informe o cpf" }]}>
                  <Input style={{ outline: "none" }}></Input>
                </Form.Item>
                <Form.Item name="name" label="Nome" rules={[{ required: true, message: "Informe o nome" }]}>
                  <Input style={{ outline: "none" }}></Input>
                </Form.Item>
                <Form.Item name="password" label="Senha" rules={[{ required: true, message: "Informe a senha" }]}>
                  <Input.Password style={{ outline: "none" }}
                    iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
                </Form.Item>
                <Row>
                  <Col span={12}>
                    <Form.Item>
                      <Button style={{ width: '100%' }} size="large" type='danger' onClick={reset}>
                        Cancelar
                      </Button>
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item>
                      <Button style={{ marginLeft: "10px", width: '100%' }} size="large" type='primary' htmlType="submit">
                        Cadastrar
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        }
        {!signup &&
          <Row>
            {!succesLogin &&
              <Col span={24}>
                <h2>Login/Senha incorretos!</h2>
              </Col>
            }
            <Col span={24}>
              <Form layout="vertical" name="authentication" onFinish={onFinish}>
                <Form.Item name="cpf" label="Cpf" rules={[{ required: true, message: "Informe o cpf" }]}>
                  <Input style={{ outline: "none" }}></Input>
                </Form.Item>
                <Form.Item name="password" label="Senha" rules={[{ required: true, message: "Informe a senha" }]}>
                  <Input.Password style={{ outline: "none" }}
                    iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
                </Form.Item>
                <Row>
                  <Col span={12}>
                    <Form.Item>
                      <Button style={{ width: '100%' }} size="large" onClick={e => setSignup(true)}>
                        Cadastrar
                      </Button>
                    </Form.Item>
                  </Col>
                  <Col span={12}>
                    <Form.Item>
                      <Button style={{ marginLeft: "10px", width: '100%' }} size="large" type="primary" htmlType="submit">
                        Entrar
                      </Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Col>
          </Row>
        }
      </Card>
    </Row>
  );
}

export default Login;
