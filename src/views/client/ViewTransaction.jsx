import { Row, Col, Card, Table, Button } from 'antd'
import { getMoney } from '../../services/user'
import { getTransaction, downloadCSV } from '../../services/transaction'
import { getCategories } from '../../services/category'
import React, { useState, useEffect } from 'react'
import { DatePicker, Space } from 'antd';
const { RangePicker } = DatePicker;

const ViewTransaction = () => {
  const [money, setMoney] = useState(false);
  const id = JSON.parse(localStorage.getItem("credentials")).id;
  const [transactions, setTransaction] = useState(false);
  const [csv, setCsv] = useState("");

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name"
    },
    {
      title: "Valor",
      dataIndex: "transferredMoney",
      key: "transferredMoney"
    },
    {
      title: "Observação",
      dataIndex: "obs",
      key: "obs"
    },
    {
      title: "Categoria",
      dataIndex: "type",
      key: "type"
    },
    {
      title: "Data",
      dataIndex: "created_at",
      key: "created_at"
    },
    {
      title: "Categoria",
      dataIndex: "category",
      key: "category"
    },
  ]


  useEffect(async () => {
    let categories;
    await getCategories()
      .then(response => {
        if (response.data.message == "Nenhuma categoria encontrada!")
          categories = false;
        else
          categories = response.data;
      })
      .catch(error => {
        console.log(error);
        categories = false;
      });

    await getMoney()
      .then(response => {
        setMoney(response.data.money);
      })
      .catch(error => {
        console.log(error);
        setMoney("Erro ao recuperar o saldo")
      })
      
    await getTransaction()
      .then(response => {
        response.data.forEach(transcation => {
          transcation['type'] = id === transcation.idUserWhoSent ? "Enviado" : "Recebido";
          transcation['transferredMoney'] = id === transcation.idUserWhoSent ?
            transcation['transferredMoney'] : -transcation['transferredMoney'];
          transcation['name'] = id === transcation.idUserWhoSent ?
            transcation['nameUserWhoReceived'] : transcation['nameUserWhoSent'];
          transcation["created_at"] = transcation["created_at"].substr(0, 10);
          if (transcation["category"])
            transcation["category"] = categories.find(category => category.id == transcation["category"]).name
        })
        setTransaction(response.data);
      })
      .catch(error => {
        console.log(error);
        setTransaction(false);
      })

  }, [])

  const handleDownload = async () => {
    await downloadCSV(transactions)
      .then(response => {
        setCsv(response.data.message);
      }) 
      .catch(error => {
        setCsv("");
        console.log(error)
      })
  }

  const handleFilter = async (selectedRange) => {
    console.log(selectedRange[0]._d)
    await getTransaction(selectedRange[0]._d.toString(), selectedRange[1]._d.toString())
      .then(response => {
        response.data.forEach(transcation => {
          transcation['type'] = id === transcation.idUserWhoSent ? "Enviado" : "Recebido";
          transcation['transferredMoney'] = id === transcation.idUserWhoSent ?
            transcation['transferredMoney'] : -transcation['transferredMoney'];
          transcation['name'] = id === transcation.idUserWhoSent ?
            transcation['nameUserWhoReceived'] : transcation['nameUserWhoSent'];
          transcation["created_at"] = transcation["created_at"].substr(0, 10);
        })
        setTransaction(response.data);
      })
      .catch(error => {
        console.log(error);
        setTransaction(false);
      })
  }

  return (
    <>
      <Row span={24}>
        <Col span={18}>
        </Col>
        <Col span={6}>
          <Card size="small" style={{ marginBottom: "10px", width: "100%" }} disabled={true}>
            <p style={{ padding: "0px", margin: "0px" }}>
              Saldo: {money}
            </p>
          </Card>
        </Col>
      </Row>
      <Space direction="vertical" size={12}>
        <RangePicker placeholder={["Data inicial", "Data final"]} onChange={handleFilter} />
      </Space>
      <Space direction="vertical" size={12}>
        <Button type='dashed' style={{marginLeft: "30px"}} onClick={handleDownload}>Baixar CSV</Button>
      </Space>
      <Space direction="vertical" size={12}>
        <p style={{marginLeft: "30px"}}>{csv}</p>
      </Space>
      <Row>
        <Table style={{ width: '100%' }} colSpan={16} dataSource={transactions} columns={columns} />
      </Row>
    </>
  )
}

export default ViewTransaction;