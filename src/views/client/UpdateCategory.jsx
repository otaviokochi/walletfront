import { Col, Form, Button, Input, Select } from 'antd'
import { getCategories, updateCategory } from '../../services/category'
import React, { useState, useEffect } from 'react'

const MakeTransaction = () => {
  const [categories, setCategories] = useState(false);
  const [responseRequisition, setResponseRequisition] = useState(false);
  const [firstRender, setFirstRender] = useState(false);
  const { Option } = Select;
  const [form] = Form.useForm();

  useEffect(async () => {
    await getCategories()
      .then(response => {
        if (response.data.message == "Nenhuma categoria encontrada!")
          setCategories(false);
        else
          setCategories(response.data);
      })
      .catch(error => {
        console.log(error);
        setCategories(false);
      });

  }, [])


  const handleFinish = async (values) => {
    setFirstRender(false);
    await updateCategory(values)
      .then(response => {
        console.log(response);
        setResponseRequisition(response.data.message);
        form.resetFields();
      })
      .catch(error => {
        console.log(error);
        if (error && error.response && error.response.data && error.response.data.message) {
          console.log(error.response.data.message);
          setResponseRequisition(error.response.data.message);
        }
        else {
          setResponseRequisition("Erro ao realizar transação!");
        }
      })
  }

  return (
    <>
      <Form form={form} onFinish={handleFinish}>
        <Col>
          <Form.Item style={{ width: "100%" }} name="oldCategory" label="Categoria">
            <Select
              placeholder="Categoria"
              style={{ width: "100%" }}
            >
              {categories && categories.map((category) => (
                <Option value={category.name}>{category.name}</Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item rules={[{ required: true, message: "Informe o novo nome" }]} name="updatedCategory" label="Novo nome">
            <Input type="string" style={{ outline: "none" }}></Input>
          </Form.Item>
          <Col span={24}>
            <Form.Item>
              <Button style={{ height: "40px", float: "right" }} size="large" type="primary" htmlType="submit">
                Atualizar
              </Button>
            </Form.Item>
          </Col>
        </Col>
      </Form>
      {!firstRender &&
        <p style={{ color: "red" }}>{responseRequisition}</p>
      }
    </>

  )
}

export default MakeTransaction;