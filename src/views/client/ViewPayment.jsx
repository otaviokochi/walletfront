import { Row, Col, Card, Table, Button } from 'antd'
import { getMoney } from '../../services/user'
import { getPayment, pay, chargeBack } from '../../services/payment'
import React, { useState, useEffect } from 'react'
import { DatePicker, Space } from 'antd';
const { RangePicker } = DatePicker;

const ViewTransaction = () => {
  const [money, setMoney] = useState(false);
  const id = JSON.parse(localStorage.getItem("credentials")).id;
  const [payments, setPayments] = useState(false);
  const [responseServer, setResponseServer] = useState(false);

  const handlePayment = async (values) => {
    await pay(values.id)
      .then(response => {
        console.log(response);
        setResponseServer(response.data.message)
      })
      .catch(error => {
        console.log(error)
        if (error && error.response && error.response.data && error.response.data.message)
          setResponseServer(error.response.data.message)
        else
          setResponseServer("Erro ao realizar pagamento!")
      })
  }

  const handleChargeBack = async (values) => {
    await chargeBack(values.id)
      .then(response => {
        console.log(response);
        setResponseServer(response.data.message)

      })
      .catch(error => {
        console.log(error)
        if (error && error.response && error.response.data && error.response.data.message)
          setResponseServer(error.response.data.message)
        else
          setResponseServer("Erro ao realizar pagamento!")
      })
  }



  const columns = [
    {
      title: "Vencimento",
      dataIndex: "dueDate",
      key: "dueDate"
    },
    {
      title: "Periocidade",
      dataIndex: "periodicity",
      key: "periodicity"
    },
    {
      title: "Débito automático",
      dataIndex: "automaticPayment",
      key: "automaticPayment"
    },
    {
      title: "Quantia",
      dataIndex: "paymentMoney",
      key: "paymentMoney"
    },
    {
      title: "Destinatário",
      dataIndex: "nameUserWhoReceived",
      key: "nameUserWhoReceived"
    },
    {
      title: "Remetente",
      dataIndex: "nameUserWhoSent",
      key: "nameUserWhoSent"
    },
    {
      title: "Repetirá quantas vezes",
      dataIndex: "timesWillBeRepeated",
      key: "timesWillBeRepeated"
    },
    {
      title: "Já foi repetido quantas vezes",
      dataIndex: "timesExecuted",
      key: "timesExecuted"
    },
    {
      title: 'Pagar',
      key: 'pay',
      dataIndex: 'pay',
      render: (_, record) => (
        <>
          { record.isActive && record.idUserWhoSent == id &&
            <Button onClick={e => handlePayment(record)}>
              Pagar
          </Button>
          }
        </>
      ),
    },
    {
      title: 'Estorno',
      key: 'chargeBack',
      dataIndex: 'chargeBack',
      render: (_, record) => (
        <>
          { record.isActive && record.idUserWhoSent == id &&
            <Button onClick={e => handleChargeBack(record)}>
              Estornar
          </Button>
          }
        </>
      ),
    },
  ]


  useEffect(async () => {
    await getMoney()
      .then(response => {
        setMoney(response.data.money);
      })
      .catch(error => {
        console.log(error);
        setMoney("Erro ao recuperar o saldo")
      })

    await getPayment()
      .then(response => {
        console.log(response.data);

        response.data.forEach(payment => {
          payment['automaticPayment'] = payment['automaticPayment'] ? "Sim" : "Não";
        })
        setPayments(response.data);
      })
      .catch(error => {
        console.log(error);
        setPayments(false);
      })

  }, [])

  return (
    <>
      <Row span={24}>
        <Col span={18}>
        </Col>
        <Col span={6}>
          <Card size="small" style={{ marginBottom: "10px", width: "100%" }} disabled={true}>
            <p style={{ padding: "0px", margin: "0px" }}>
              Saldo: {money}
            </p>
          </Card>
        </Col>
      </Row>
      <Row>
        <Table style={{ width: '100%' }} colSpan={16} dataSource={payments} columns={columns} />
      </Row>
      <Row>
        {responseServer &&
          <p>{responseServer}</p>
        }
      </Row>
    </>
  )
}

export default ViewTransaction;