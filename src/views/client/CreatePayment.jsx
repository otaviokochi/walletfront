import React, { useState } from 'react'
import { createPayment } from '../../services/payment'
import { Row, DatePicker, Col, Form, Button, Input, Select } from 'antd'

const ViewTransaction = () => {
  const [response, setResponse] = useState(false);
  const { Option } = Select;
  let date;
  
  const onFinish = async (values) => {
    
    values.dueDate = date;
    await createPayment(values)
      .then(response => {
        setResponse(response.data.message);
      })
      .catch(error => {
        if (error && error.response && error.response.data) {
          console.log(error.response.data.message)
          setResponse(error.response.data.message);
        }
        else {
          setResponse("Erro ao criar categoria");
        }

      })
  }

  const changeDate = (_, dueDate) => {
    date = dueDate;
  }

  const plans = [
    {
      value: 1,
      name: "Diário"
    },
    {
      value: 2,
      name: "Semanal"
    },
    {
      value: 3,
      name: "Quinzenal"
    },
    {
      value: 4,
      name: "Semestral"
    },
    {
      value: 5,
      name: "Anual"
    },
  ]

  return (
    <>
      <Form onFinish={onFinish}>
        <Row span={24}>
          <Col span={18}>
            <Form.Item name="cpf" label="Cpf para quem deseja realizar o pagamento" rules={[{ required: true, message: "Informe o cpf" }]}>
              <Input type="number" style={{ outline: "none" }} placeholder="Apenas números"></Input>
            </Form.Item>
            <Form.Item name="paymentMoney" label="Dinheiro" rules={[{ required: true, message: "Informe o dinheiero" }]}>
              <Input type="number" style={{ outline: "none" }}></Input>
            </Form.Item>
            <Form.Item name="periodicity" label="Plano" rules={[{ required: true, message: "Informe o plano" }]}>
              <Select
                style={{ width: "100%" }}
              >
                {plans && plans.map((plan) => (
                  <Option value={plan.value}>{plan.name}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="timesWillBeRepeated" label="Repetir quantas vezes" rules={[{ required: true, message: "Informe repetição" }]}>
              <Input type="number" style={{ outline: "none" }}></Input>
            </Form.Item>
            <Form.Item name="automaticPayment" label="Débito automático?" rules={[{ required: true, message: "Informe o campo" }]}>
              <Select
                style={{ width: "100%" }}>
                <Option value={true}>Sim</Option>
                <Option value={false}>Não</Option>
              </Select>
            </Form.Item>
            <Form.Item name="dueDate" label="Vencimento" rules={[{ required: true, message: "Informe o vencimento" }]}>
              <DatePicker onChange={changeDate}/>
            </Form.Item>
          </Col>
          <Col>
            <Button style={{ marginLeft: "15px" }} type="primary" htmlType="submit">
              Criar
            </Button>
          </Col>
        </Row>
        <Row>
          {response &&
            <p>{response}</p>
          }
        </Row>
      </Form>
    </>
  )
}

export default ViewTransaction;