import { Row, Col, Card, Form, Button, Input, Select } from 'antd'
import { getCategories } from '../../services/category'
import React, { useState, useEffect } from 'react'
import { getMoney } from '../../services/user'
import {newTransaction} from '../../services/transaction'

const MakeTransaction = () => {
  const [categories, setCategories] = useState(false);
  const [money, setMoney] = useState(false);
  const [responseRequisition, setResponseRequisition] = useState(false);
  const [firstRender, setFirstRender] = useState(false);
  const { Option } = Select;
  const [form] = Form.useForm();

  useEffect(async () => {
    await getCategories()
      .then(response => {
        if (response.data.message == "Nenhuma categoria encontrada!")
          setCategories(false);
        else
          setCategories(response.data);
      })
      .catch(error => {
        console.log(error);
        setCategories(false);
      });

    await getMoney()
      .then(response => {
        setMoney(response.data.money);
      })
      .catch(error => {
        console.log(error);
        setMoney("Erro ao recuperar o saldo")
      })

  }, [])


  const handleFinish = async (values) => {
    console.log(values)
    setFirstRender(false);
    await newTransaction(values)
      .then(response => {
        console.log(response);
        setResponseRequisition(response.data.message);
        form.resetFields();
      })
      .catch(error => {
        console.log(error);
        if (error && error.response && error.response.data && error.response.data.message){
          console.log(error.response.data.message);
          setResponseRequisition(error.response.data.message);
        }
        else {
          setResponseRequisition("Erro ao realizar transação!");
        }
      })
  }

  return (
    <>
      <Row span={24}>
        <Col span={18}>
        </Col>
        <Col span={6}>
          <Card size="small" style={{ marginBottom: "10px", width: "100%" }} disabled={true}>
            <p style={{ padding: "0px", margin: "0px" }}>
              Saldo: {money}
            </p>
          </Card>
        </Col>
      </Row>
      <Form form={form} onFinish={handleFinish}>
        <Col>
          <Form.Item name="cpf" label="Cpf para quem deseja transferir" rules={[{ required: true, message: "Informe o cpf" }]}>
            <Input type="number" style={{ outline: "none" }} placeholder="Apenas números"></Input>
          </Form.Item>
          <Form.Item name="money" label="Dinheiro a ser transferido" rules={[{ required: true, message: "Informe o dinheiero" }]}>
            <Input type="number" style={{ outline: "none" }}></Input>
          </Form.Item>
          <Form.Item name="obs" label="Observação">
            <Input type="string" style={{ outline: "none" }}></Input>
          </Form.Item>
          <Form.Item name="category" label="Categoria">
            <Select
              placeholder="Categoria"
              style={{ width: "100%" }}
            >
              {categories && categories.map((category) => (
                <Option value={category.id}>{category.name}</Option>
              ))}
            </Select>
          </Form.Item>
          <Col span={24}>
            <Form.Item>
              <Button style={{ height: "40px", float: "right" }} size="large" type="primary" htmlType="submit">
                Enviar
              </Button>
            </Form.Item>
          </Col>
        </Col>
      </Form>
      {!firstRender &&
        <p style={{color: "red"}}>{responseRequisition}</p>
      }
    </>

  )
}

export default MakeTransaction;