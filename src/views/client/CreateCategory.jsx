import React, { useState} from 'react'
import { createCategory } from '../../services/category'
import { Row, Col, Form, Button, Input} from 'antd'

const ViewTransaction = () => {
  const [response, setResponse] = useState(false);

  const onFinish = async (values) => {
    await createCategory(values)
      .then(response => {
        setResponse(response.data.message);
      })
      .catch(error => {
        if (error && error.response && error.response.data) {
          console.log(error.response.data.message)
          setResponse(error.response.data.message);
        }
        else {
          setResponse("Erro ao criar categoria");
        }

      })
  }

  return (
    <>
      <Form onFinish={onFinish}>
        <Row span={24}>
          <Col span={18}>
            <Form.Item name="category" label="Categoria">
              <Input type="string" style={{ outline: "none" }}></Input>
            </Form.Item>
          </Col>
          <Col>
            <Button style={{ marginLeft: "15px" }} type="primary" htmlType="submit">
              Criar
            </Button>
          </Col>
        </Row>
        <Row>
          {response &&
            <p>{response}</p>
          }
        </Row>
      </Form>
    </>
  )
}

export default ViewTransaction;